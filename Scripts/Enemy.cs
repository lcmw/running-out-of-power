﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : SolarPower {
	
	private Renderer renderer;

	private Vector3 point;
	private bool move = false;
	private bool moveBack = false;
	private float nt;

	private void Start() {
		base.lossRate = 0.25f;
		renderer = GetComponent<Renderer>();
	}

	public override void OnDie() {
		Destroy(gameObject);
		Destroy(this);
	}

	public override void OnSunlightSupplyChange(bool isInSun) {
		if(renderer == null) return;
		if (!isInSun)
			renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, 0.5f);
		else
			renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, 1);
	}

	public override void UpdateThis() {
		if (Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, gameObject.transform.position) < 50
			&& Vector3.Distance(GameObject.FindGameObjectWithTag("Player").transform.position, gameObject.transform.position) > 5) {
			Vector3 pos = gameObject.transform.position;
			Vector3 ppos = GameObject.FindGameObjectWithTag("Player").transform.position;
			
			gameObject.transform.position = Vector3.MoveTowards(pos, ppos, 0.1f);
		}


			if(Time.time > nt) {
				nt = Time.time + 5f;
				GetComponent<Gun>().fire(false);
			}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {
			point = new Vector3(transform.position.x - 50, transform.position.y, transform.position.z - 50);
		}
		
		if (other.gameObject.tag.Equals("Blocker") && GetComponent<BoxCollider>()) {
			isInSun = false;
			OnSunlightSupplyChange(false);
		}
	}

	public void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player") {
			moveBack = false;
		}
		if (other.gameObject.tag.Equals("Blocker") && !isInSun && GetComponent<BoxCollider>()) {
			isInSun = true;
			OnSunlightSupplyChange(true);
		}
	}
}
