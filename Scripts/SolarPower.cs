﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SolarPower : MonoBehaviour {
	public bool isInSun = true;
	public float batteryLife = 100;
	public float lossRate = 5f;

	public Vector3 lowest, highest, offset;
	public float speed = 0.005f;

	public bool IsInSun { get { return isInSun; } set { isInSun = value; } }

	float originalY;

	public float floatStrength = 0.000000001f; // You can change this in the Unity Editor to 
									// change the range of y positions that are possible.
			
	private float delay;

	void Start()
	{
		delay = UnityEngine.Random.Range(0.5f, 1.5f);
		this.originalY = this.transform.position.y;
	}

	public void Damage(float amt) {
		batteryLife -= amt;
	}

	public abstract void UpdateThis();
	public abstract void OnDie();
	public abstract void OnSunlightSupplyChange(bool isInSun);

	private float time;
	private bool delayed = false;

	public void Update() {
		time += Time.deltaTime;

		if (time >= delay) {
			delayed = true;
		}

		if(delayed)
			transform.position = new Vector3(transform.position.x,
				originalY + ((float)Math.Sin(Time.time + delay) * floatStrength),
				transform.position.z);

		UpdateThis();
		if(batteryLife <= 0) {
			OnDie();
			return;
		}
	}

	public void FixedUpdate() {
		if(!isInSun) batteryLife -= lossRate;
	}

	public void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag.Equals("Blocker")) {
			isInSun = false;
			OnSunlightSupplyChange(false);
		}
	}

	public void OnTriggerExit(Collider other) {
		 if (other.gameObject.tag.Equals("Blocker") && !isInSun) {
			isInSun = true;
			OnSunlightSupplyChange(true);
		}
	}
}
