﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

	public int level;

	void OnTriggerEnter(Collider other) {
		if(other.tag == "Player")
			UnityEngine.SceneManagement.SceneManager.LoadScene (level);
	}
}
