﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : SolarPower {
	private float nextFire, nextRegen;

	public override void OnDie() {
		UnityEngine.SceneManagement.SceneManager.LoadScene (9);
	}

	public override void OnSunlightSupplyChange(bool isInSun) {
	}

	public override void UpdateThis() {
		if(Input.GetButtonDown("Fire1") && Time.time > nextFire) {
			nextFire = Time.time + 0.25f;
			GetComponent<Gun>().fire(true);
		}

		if(Time.time > nextRegen) {
			nextRegen = Time.time + 10f;
			batteryLife += 5;
		}
	}
}
