﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class howtoplay : MonoBehaviour {

	public GameObject one, two, three, four;
	public GameObject diagram1, diagram2;

	int index = 0;

	void Start () {
		
	}
	
	void Update () {
		if(Input.GetKeyUp(KeyCode.Space)) {
			if (index == 0) {
				one.SetActive(false);
				two.SetActive(true);
				diagram1.SetActive(true);
				index++;
			}
			else if (index == 1) {
				two.SetActive(false);
				three.SetActive(true);
				diagram1.SetActive(false);
				diagram2.SetActive(true);
				index++;
			}
			else if (index == 2) {
				three.SetActive(false);
				four.SetActive(true);
				diagram2.SetActive(false);
				index++;
			}
			else if(index == 3) { 
				UnityEngine.SceneManagement.SceneManager.LoadScene(2);
			}
		}
		
	}
}
