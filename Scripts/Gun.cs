﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {
	public GameObject shot;
	public GameObject shootFrom;
	public float cost, hitRange;

	private WaitForSeconds shotLife = new WaitForSeconds(0.5f);
	private AudioSource gunShotSound;
	private LineRenderer debugLine;

	private RaycastHit hit;
	private Transform point;



	public void Start() {
		gunShotSound = GetComponent<AudioSource>();
		debugLine = GetComponent<LineRenderer>();
	}

	public void fire(bool player) {
			StartCoroutine(ShotEffect());

			Vector3 rayOrigin;
			if(player) rayOrigin = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0.0f));
			else rayOrigin = shootFrom.transform.position;

			debugLine.SetPosition(0, shootFrom.transform.position);
			GetComponent<SolarPower>().Damage(cost);

			Vector3 direction;
			if(player) direction = Camera.main.transform.forward;
			else direction = shootFrom.transform.forward;

			if (Physics.Raycast(rayOrigin, direction, out hit, hitRange)) {
				SolarPower power = hit.collider.GetComponent<SolarPower>();
				if(power != null) shot = power.gameObject;
				if(power != null) StartCoroutine(EffectTarget(power));
				debugLine.SetPosition(1, hit.point);
			} else {
				debugLine.SetPosition(1, rayOrigin + (direction * hitRange));
			}
	}

	private IEnumerator ShotEffect() {
		gunShotSound.Play();
		debugLine.enabled = true;
		yield return shotLife;
		debugLine.enabled = false;
	}

	public IEnumerator EffectTarget(SolarPower power) {
		if(power.IsInSun) {
			power.OnSunlightSupplyChange(false);
			power.IsInSun = false;
		}
			
		yield return new WaitForSeconds(2);
		
		if(!power.IsInSun) {
			power.OnSunlightSupplyChange(true);
			power.IsInSun = true;
		}
	}
}
