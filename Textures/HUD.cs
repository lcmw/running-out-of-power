﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour {

	public GameObject player, txt, txt2;

	// Use this for initialization
	void Start () {
	}
	



	// Update is called once per frame
	void Update () {
		if (!player.GetComponent<Player>().isInSun)
			txt.GetComponent<UnityEngine.UI.Text>().color = new Color(1, 0, 0, 1);
		else
			txt.GetComponent<UnityEngine.UI.Text>().color = new Color(0, 1, 0, 1);

		if(GetComponent<Gun>().shot != null)txt2.GetComponent<UnityEngine.UI.Text>().text = GetComponent<Gun>().shot.GetComponent<SolarPower>().batteryLife.ToString();
		txt.GetComponent<UnityEngine.UI.Text>().text = player.GetComponent<Player>().batteryLife.ToString();
	}
}
